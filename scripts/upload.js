const fs = require('fs')
const Mammoth = require('mammoth')
const Puppeteer = require('puppeteer')
const Beautify = require('js-beautify')

// get information from .env file
const { SITE, USERNAME, PASSWORD, COURSE_ID } = JSON.parse(fs.readFileSync('.env.json', { encoding: 'utf8' }))

// convert word doc to html and split html by h2's
async function convertDoc(filePath) {

  // convert to html
  let html = await Mammoth.convertToHtml( { path: filePath }, { styleMap: ["p[style-name='Title'] => h1:fresh"] })

  // clean up html
  html = html.value
    // remove internal word links and force new page on external links
    .replace(/<a href="#(.*?)>(.*?)<\/a>/g, '$2')
    .replace(/<a id="_(.*?)>(.*?)<\/a>/g, '$2')
    .replace(/<a href="http/g, '<a class="external-link" target="_blank" href="http')

    // remove base64 image urls and replace with dummy image
    .replace(/<img src="data:image\/([\s\S]*?)\/>/g, '<img src="/fake-image.png" alt="Give proper image source and change this alt text."/>')

    // lower all headings by one
    .replace(/<(\/?)h4(.*?)>/g, '<$1h5$2>')
    .replace(/<(\/?)h3(.*?)>/g, '<$1h4$2>')
    .replace(/<(\/?)h2(.*?)>/g, '<$1h3$2>')
    .replace(/<(\/?)h1(.*?)>/g, '<$1h2$2>')

  // beautify html
  html = Beautify.html(html)

  // find indices of h2's
  let match
  const indices = [], re = RegExp('<h2', 'g')

  while (match = re.exec(html)) indices.push(match.index)
  if (indices[0] != 0) indices.splice(0, 0, 0)
  indices.push(html.length)
  indices.sort((a, b) => a - b)

  // map h2 indices to content pages
  let pages = []
  pages[0] = {
    title: 'Module outline',
    content: html.substring(indices[0], indices[1]).replace(/<h2>(.*?)<\/h2>/, '')
  }

  for (let i = 1; i < indices.length - 1; i++) {
    const pageText = html.substring(indices[i], indices[i + 1])
    pages[i] = {
      title: (pageText.match(/<h2>(.*?)<\/h2>\n/) || ['NO NAME'])[0].replace(/<h2>(.*?)<\/h2>\n/, '$1'),
      content: pageText.replace(/<h2>(.*?)<\/h2>\n/, '')
    }
  }
  return pages
}

// log in, go to course, and then upload for each module
async function uploadDocs(username, password, courseUrl) {
  const browser = await Puppeteer.launch({ headless: false, defaultViewport: null, ignoreHTTPSErrors: true })
  const p = await browser.newPage()

  await p.goto(courseUrl)
  await p.type('#username', username)
  await p.type('#password', password)
  await p.click('#loginbtn')

  await p.waitForSelector('#nav-drawer')
  const sections = await p.$eval(
    '#nav-drawer',
    n => [...n.querySelectorAll('a.list-group-item[href*="section"]')].map(a => ({ title: a.innerText.trim(), url: a.href }))
  )

  for (const section of sections) {
    if (!fs.existsSync('uploads/' + section.title + '.docx')) continue

    let activities = await convertDoc('uploads/' + section.title + '.docx')

    for (const activity of activities) {
      // convert section url to add-page-for-that-section url
      await p.goto(section.url.replace(/view\.php\?id=(\d+)&section=(\d+)/, 'modedit.php?add=page&type=&course=$1&section=$2&return=0&sr=$2'))
      await p.waitForSelector('#id_name')
      await p.$eval('#id_name', (input, content) => { input.value = content}, activity.title || 'NO NAME')
      await p.$eval('#id_page', (input, content) => { input.value = content}, activity.content || '<p>empty</p>')
      await p.click('#id_submitbutton2')
      await p.waitForNavigation()
    }
  }

  await browser.close()
}

// run everything
(async () => {
  try {
    await uploadDocs(USERNAME, PASSWORD, SITE + '/course/view.php?id=' + COURSE_ID)
    console.log('All done.')
  } catch (e) {
    console.log(e)
  }
})()
