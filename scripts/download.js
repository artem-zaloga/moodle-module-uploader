const fs = require('fs')
const Puppeteer = require('puppeteer')

// get information from .env file
const { SITE, USERNAME, PASSWORD, COURSE_ID } = JSON.parse(fs.readFileSync('.env.json', { encoding: 'utf8' }))

// download course activities html
async function downloadCourse(username, password, courseUrl) {
  try {
    const browser = await Puppeteer.launch({ headless: false, defaultViewport: null, ignoreHTTPSErrors: true })
    const p = await browser.newPage()

    await p.goto(courseUrl)
    await p.type('#username', username)
    await p.type('#password', password)
    await p.click('#loginbtn')

    await p.waitForSelector('#nav-drawer')
    const sections = await p.$eval(
      '#nav-drawer',
      n => [...n.querySelectorAll('a.list-group-item[href*="section"]')].map(a => ({ title: a.innerText.trim(), url: a.href }))
    )

    for (const section of sections) {
      await p.goto(section.url)
      if (!(await p.$('ul.section'))) continue

      const activities = await p.$eval(
        'ul.section',
        el => [...el.querySelectorAll('li.activity a[href*="/mod/"]:first-child')].map(a => ({ title: a.innerText.trim(), url: a.href }))
      )

      let moduleContent = ''

      for (const activity of activities) {
        moduleContent += `<h2 class="mt-4"><a target="_blank" href="${activity.url}">${activity.title}</a></h2>`

        // deal with non-page activities later
        if (!activity.url.includes('/page/')) continue

        await p.goto(activity.url.replace(/mod.*?id=(\d+)/, 'course/modedit.php?update=$1'))
        await p.waitForSelector('div[role="main"]')

        moduleContent += await p.$eval(
          'div[role="main"]',
          m => m.querySelector('#id_pageeditable').innerHTML
                .replace(/\[vue\]/g, '<div class="vue-components">')
                .replace(/\[\/vue\]/g, '</div>')
        )
      }

      fs.writeFileSync('downloads/' + section.title + '.html', `<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="https://use.typekit.net/ecd6kjj.css">
    <link rel="stylesheet" type="text/css" href="https://iasptraining.ca/moodle/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css">
    <link rel="stylesheet" type="text/css" href="https://iasptraining.ca/moodle/theme/styles.php/boostchild/1653662696_1564760135/all">
    <link href="https://moodlemedia.camhx.ca/Moodle11/component-scripts/vue-components.css" rel="stylesheet">
    <title>${section.title}</title>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery" src="https://iasptraining.ca/moodle/lib/javascript.php/1653662889/lib/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://iasptraining.ca/moodle/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.js"></script>
    <script type="text/javascript" src="https://iasptraining.ca/moodle/lib/javascript.php/1653662889/lib/javascript-static.js"></script>
    <script type="text/javascript" src="https://moodlemedia.camhx.ca/Moodle11/component-scripts/vue.min.js"></script>
    <script type="text/javascript" src="https://moodlemedia.camhx.ca/Moodle11/component-scripts/vue-components.js"></script>
    <script type="text/javascript" src="https://moodlemedia.camhx.ca/Moodle11/component-scripts/filter.js"></script>
    <script>
    window.addEventListener('DOMContentLoaded', function(){
      if (document.body.innerHTML.indexOf('vue-components') > -1) {
        var vueDivs = document.getElementsByClassName('vue-components');
        for (var i = 0; i < vueDivs.length; i++) {

          new Vue({
            el: vueDivs[i],
            data: function() {
              return {
                state: {}
              }
            }
          });
        }
      }
    });
    </script>
  </head>
  <body class="container">
    <h1><a target="_blank" href="${section.url}">${section.title}</a></h1>
    ${moduleContent}
  </body>
</html>`)
    }

    await browser.close();
  } catch (err) {
    console.error(err)
  }
}



// run everything
(async () => {
  await downloadCourse(USERNAME, PASSWORD, SITE + '/course/view.php?id=' + COURSE_ID)
  console.log('All done.')
})()
