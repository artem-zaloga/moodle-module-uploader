# Moodle Module uploader

This tool does three things:
1. takes word docs, splits them up by Header1's into pages and converts each page to html, then uploads each html page as a Page module to a Moodle course
2. downloads the html of all activities in a Moodle course and combines them into a pdf per section
3. creates ands sets up a course from an IASP course template, then adds and syncs a cohort to that course 

This spins up a headless browser and then logs in to your Moodle course with the given user credentials, so be smart about which user you're using for this process.

_Note: this probably won't work on Windows._

## Setup
```
git clone https://gitlab.com/artem-zaloga/moodle-module-uploader.git
cd moodle-module-uploader
npm install
```

Then create a file called `.env.json` with the contents:
```
{
  "SITE": "site base url (no trailing slash)",
  "USERNAME": "username",
  "PASSWORD": "password",

  "COURSE_ID": "number",

  "COURSE": "FUND, ANX0/1/2/3, DEPR, or CLIN",
  "INSTANCE": "suffix (a, b, ...) if multiple instances of same course being created on same day",
  "START": "yyyy-mm-dd",
  "END": "yyyy-mm-dd",
  "FACILITATORS": "csv of facilitator names (Firstname Lastname, ...)"
}
```

First three are required for login, `COURSE_ID` is required for upload/download scripts, and rest are required for create script.

## Upload

Add any `.docx` files that you wish to be uploaded to the `/uploads` directory, with the files having as their names the section names that they should go into (section names visible in the sidenav).

Then run `npm run upload`.

## Download (WIP)

Run `npm run download`.

Downloaded files will appear in the `/downloads` directory.

## Create

Run `npm run create`.

Url of created course will be logged out at the end of the process.

## Errors

Sometimes errors arise either from the Moodle server hanging or the Puppeteer headless browser randomly crashing. In such instances just kill the process (`Ctrl + C`) and restart the command (if uploading, delete any partially uploaded activities first).
