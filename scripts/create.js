// this is only for iasp courses
const fs = require('fs')
const Puppeteer = require('puppeteer')

// get information from .env file
const { SITE, USERNAME, PASSWORD, COURSE, INSTANCE, START, END, FACILITATORS } = JSON.parse(fs.readFileSync('.env.json', { encoding: 'utf8' }))

// just wait
const delay = async s => new Promise(r => {
  setTimeout(() => { r() }, 1000 * s)
})

async function logIn(p, username, password) {
  await p.goto(SITE + '/login/index.php')
  await p.waitForSelector('#username')

  await p.type('#username', username)
  await p.type('#password', password)
  await p.click('#loginbtn')
  await p.waitForSelector('#page-my-index')
}

async function createCourse(p, course, instance, start, end) {
  // cohort shortname to full name
  const courseFullName = {
    FUND: 'Fundamentals of CBT',
    ANX0: 'Introduction to CBT for Anxiety and Related Disorders',
    ANX1: 'CBT for Anxiety and Related Disorders 1',
    ANX2: 'CBT for Anxiety and Related Disorders 2',
    ANX3: 'CBT for Anxiety and Related Disorders 3',
    DEPR: 'CBT for Depressive Disorders',
    CLIN: 'Clinical Consultation for CBT'
  }

  // 'Originals' course category
  await p.goto(SITE + '/course/management.php?categoryid=12')
  await p.waitForSelector('.listitem-course')
  await delay(5) // slow moodle...

  await p.$$eval('.listitem-course', (courses, courseFullName) => {
    for (const c of courses) {
      if (c.textContent.includes(courseFullName)) {
        c.querySelector('.action-copy').click()
      }
    }
  }, courseFullName[course])

  await p.waitForSelector('#id_fullname')
  await p.type('#id_fullname', courseFullName[course] + '-' + start + instance)
  await p.type('#id_shortname', course + '-' + start + instance)

  await p.select('#id_visible', '0') // hide while testing

  await p.click('#id_enddate_enabled')
  await p.select('#id_enddate_day', end.slice(8).replace(/^0/, ''))
  await p.select('#id_enddate_month', end.slice(5, 7).replace(/^0/, ''))
  await p.select('#id_enddate_year', end.slice(0, 4))
  await p.click('#id_submitdisplay')

  await p.waitForSelector('td.c1 a', { timeout: 0 })

  return await p.$eval('td.c1 a', a => a.href)
}

async function createCohort(p, course, instance, start) {
  // 'Active' cohort category
  await p.goto(SITE + '/cohort/edit.php?contextid=27076')
  await p.waitForSelector('#id_name')
  await delay(5) // slow moodle...

  await p.type('#id_name', course + '-' + start + instance)
  await p.type('#id_idnumber', course + '-' + start + instance)
  await p.click('#id_submitbutton')
  await p.waitForNavigation()
}

async function syncCohort(p, course, instance, start, courseUrl) {
  const cId = (courseUrl.match(/id=(\d+)$/) || [])[1]
  
  // add cohort enrolment to course
  await p.goto(SITE + '/enrol/editinstance.php?type=cohort&courseid=' + cId)
  await p.waitForSelector('#id_name')
  await delay(5) // slow moodle...

  await p.type('#id_name', course + '-' + start + instance)
  await p.type('input[id^="form_autocomplete_input"]', course + '-' + start + instance)
  await p.waitForSelector('li[id^="form_autocomplete_suggestions"]')
  await p.click('li[id^="form_autocomplete_suggestions"]')
  await delay(5) // slow moodle...

  await p.click('#id_submitbutton')
  await p.waitForNavigation()
}

async function restrictModules(p, start, courseUrl) {
  await p.goto(courseUrl)
  await p.waitForSelector('button[title="Turn editing on"]')
  await p.click('button[title="Turn editing on"]')
  await p.waitForSelector('.section .right.side a[id^="action-menu-toggle"]')
  await delay(15) // slow moodle...

  const sectionEditUrls = await p.$$eval(
    '.section .right.side a[id^="action-menu-toggle"]', 
    links => links.map(l => l.parentElement.querySelector('a.edit').href)
  )

  for (const url of sectionEditUrls) {
    await p.goto(url)
    await delay(5)
    const sectionName = await p.$eval('#id_name_value', n => n.value)
    if (!sectionName.includes('Module ') && !sectionName.includes('Post-')) continue

    await p.click('a[aria-controls="id_availabilityconditions"]')

    await p.waitForSelector('.availability-button button')
    await p.click('.availability-button button')

    await p.waitForSelector('#availability_addrestriction_date')
    await p.click('#availability_addrestriction_date')

    await p.select('select[name="x[day]"]', start.slice(8).replace(/^0/, ''))
    await p.select('select[name="x[month]"]', start.slice(5, 7).replace(/^0/, ''))
    await p.select('select[name="x[year]"]', start.slice(0, 4))
    await delay(5)
    await p.click('#id_submitbutton')
    await p.waitForNavigation()
  }
}

async function createFacilitatorBios(p, facilitators, courseUrl) {
  // facilitator bio generator page
  await p.goto(SITE + '/mod/devpage/view.php?id=35338&facilitators=' + facilitators)
  await delay(5)

  const bios = await p.$eval('#bio-generator', n => n.textContent)

  await p.goto(courseUrl)
  await p.waitForSelector('#nav-drawer')
  await p.$$eval('#nav-drawer a.list-group-item[href*="section"]', modules => {
    for (const m of modules) {
      if (m.textContent.includes('Orientation')) m.click()
    }
  })
  await delay(5)

  const editUrl = await p.$$eval('.aalink', activities => {
    for (const a of activities) {
      if (a.textContent.includes('Facilitators')) {
        return a.href.replace('mod/page/view.php?id', 'course/modedit.php?update') + '&return=1'
      }
    }
  })
  await p.goto(editUrl)
  await delay(5)

  await p.$eval('#id_page', (n, text) => n.textContent = text, bios)
  await p.click('#id_submitbutton')
  await p.waitForNavigation()
}

// run everything
(async () => {
  let browser
  try {
    browser = await Puppeteer.launch({ headless: false, defaultViewport: null, ignoreHTTPSErrors: true })
    const page = await browser.newPage()

    await logIn(page, USERNAME, PASSWORD)
    const courseUrl = await createCourse(page, COURSE, INSTANCE, START, END)
    await createCohort(page, COURSE, INSTANCE, START)
    await syncCohort(page, COURSE, INSTANCE, START, courseUrl)
    await restrictModules(page, START, courseUrl)

    if (COURSE != 'ANX0') await createFacilitatorBios(page, FACILITATORS, courseUrl)

    console.log('Created: ' + courseUrl)
  } catch (e) {
    console.log(e)
  } finally {
    await browser.close()
  }
})()
